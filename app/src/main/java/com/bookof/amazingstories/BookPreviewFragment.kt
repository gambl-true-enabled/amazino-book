package com.bookof.amazingstories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.book_preview_fragment.*


class BookPreviewFragment(private val idBook: Int) : Fragment() {

    private val images = listOf(
        R.drawable.image1,
        R.drawable.image2,
        R.drawable.image3,
        R.drawable.image4,
        R.drawable.image5,
        R.drawable.image6
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.book_preview_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val names = resources.getStringArray(R.array.names_book)
        val authors = resources.getStringArray(R.array.authors_book)
        val descriptions = resources.getStringArray(R.array.descriptions_book)

        preview_name_book.text = names[idBook]
        preview_author_book.text = authors[idBook]
        preview_description.text = descriptions[idBook]
        preview_image.setImageDrawable(activity!!.resources.getDrawable(images[idBook]))
        preview_button.background = activity!!.resources.getDrawable(R.drawable.ic_blue_button)
        preview_button_text.text = "Read"

        preview_button.setOnClickListener {
            (activity as BookActivity).openBookReaderFragment(idBook)
        }
    }

}