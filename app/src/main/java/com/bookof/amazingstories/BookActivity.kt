package com.bookof.amazingstories

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager

class BookActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                BookListFragment()
            ).commit()

    }

    fun openBookPreviewFragment(idBook: Int) {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                BookPreviewFragment(idBook)
            ).addToBackStack(null).commit()
    }

    fun openBookReaderFragment(idBook: Int) {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                ReaderFragment(idBook)
            ).addToBackStack(null).commit()
    }

}