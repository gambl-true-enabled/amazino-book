package com.bookof.amazingstories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.barteksc.pdfviewer.util.FitPolicy
import kotlinx.android.synthetic.main.book_reader_fragment.*

class ReaderFragment(private val idBook: Int) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.book_reader_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pdfNames = resources.getStringArray(R.array.books_name_pdf)
        val namePdf = pdfNames[idBook]
        pdf_view.fromStream(activity!!.assets.open(namePdf))
            .enableSwipe(true) // allows to block changing pages using swipe
            .swipeHorizontal(false)
            .enableDoubletap(true)
            .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
            .password(null)
            .scrollHandle(null)
            .enableAntialiasing(true)
            .pageFitPolicy(FitPolicy.WIDTH)// improve rendering a little bit on low-res screens
            .load();
    }
}