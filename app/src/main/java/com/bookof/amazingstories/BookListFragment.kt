package com.bookof.amazingstories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.book_list_fragment.*


class BookListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.book_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        wallpaper_book1.setOnClickListener{
            openBookPreview(0)
        }
        wallpaper_book2.setOnClickListener{
            openBookPreview(1)
        }
        wallpaper_book3.setOnClickListener{
            openBookPreview(2)
        }
        wallpaper_book4.setOnClickListener{
            openBookPreview(3)
        }
        wallpaper_book5.setOnClickListener{
            openBookPreview(4)
        }
        wallpaper_book6.setOnClickListener{
            openBookPreview(5)
        }
    }

    private fun openBookPreview(id: Int){
        (activity as BookActivity).openBookPreviewFragment(id)
    }
}